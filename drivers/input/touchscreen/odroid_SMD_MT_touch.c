//[*]--------------------------------------------------------------------------------------------------[*]
//
//
// 
//  HardKernel(HKC1XX) Touch driver (charles.park)
//  2009.07.22
// 
//
//[*]--------------------------------------------------------------------------------------------------[*]
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/device.h>
#include <linux/input.h>
#include <linux/delay.h>
#include <linux/fs.h>

#include <mach/irqs.h>
#include <asm/system.h>

#include <asm/gpio.h>
#include <plat/gpio-cfg.h>
#include <mach/regs-gpio.h>
#include <mach/gpio-bank.h>

//[*]--------------------------------------------------------------------------------------------------[*]
#ifdef CONFIG_HAS_EARLYSUSPEND
	#include <linux/wakelock.h>
	#include <linux/earlysuspend.h>
	#include <linux/suspend.h>
#endif

//[*]--------------------------------------------------------------------------------------------------[*]
#include "hkc1xx_touch.h"
#include "hkc1xx_touch_gpio_i2c.h"
#include "hkc1xx_touch_sysfs.h"

#include "qt602240.h"
//[*]--------------------------------------------------------------------------------------------------[*]
//#define	DEBUG_HKC1XX_TOUCH_MSG
#define	DEBUG_HKC1XX_TOUCH_PM_MSG
//[*]--------------------------------------------------------------------------------------------------[*]
// Touch Key define
//[*]--------------------------------------------------------------------------------------------------[*]
#define	MAX_KEYCODE_CNT		3

int HKC1XX_TouchKeycode[MAX_KEYCODE_CNT] = {
		KEY_BACK,		KEY_MENU,		KEY_HOME 		
};

#if	defined(DEBUG_HKC1XX_TOUCH_MSG)
	const char HKC1XX_TouchKeyMapStr[MAX_KEYCODE_CNT][20] = {
		"KEY_BACK\n", 	"KEY_MENU\n",	"KEY_HOME\n"
	};
#endif	// DEBUG_MSG

#define	TS_IRQ_PORT		(S5PV210_GPH1(0))
#define	TS_IRQ_STR		"TS IRQ"

#define	TS_KEYSIG_PORT	(S5PV210_GPH2(2))
#define	TS_KEYSIG_STR	"KEY SIG"

//[*]--------------------------------------------------------------------------------------------------[*]
hkc1xx_touch_t	hkc1xx_touch;

//[*]--------------------------------------------------------------------------------------------------[*]
extern	qt602240_t		qt602240;

//[*]--------------------------------------------------------------------------------------------------[*]
static void 			hkc1xx_touch_keysignal_timer_start	(void);
static void				hkc1xx_touch_keysignal_timer_irq	(unsigned long arg);

static void 			hkc1xx_touch_timer_start	(void);
static void				hkc1xx_touch_timer_irq		(unsigned long arg);

static void				hkc1xx_touch_get_key_data	(unsigned char in_data);
static void 			hkc1xx_touch_get_data		(unsigned int id);
static void				hkc1xx_touch_input_report	(unsigned int id);

irqreturn_t				hkc1xx_touch_irq			(int irq, void *dev_id);

static int              hkc1xx_touch_open			(struct input_dev *dev);
static void             hkc1xx_touch_close			(struct input_dev *dev);

static void             hkc1xx_touch_release_device	(struct device *dev);

#ifdef CONFIG_HAS_EARLYSUSPEND
	static void				hkc1xx_touch_early_suspend	(struct early_suspend *h);
	static void				hkc1xx_touch_late_resume	(struct early_suspend *h);
#else
	static int              hkc1xx_touch_resume			(struct platform_device *dev);
	static int              hkc1xx_touch_suspend		(struct platform_device *dev, pm_message_t state);
#endif

static void				hkc1xx_touch_reset			(void);
static void 			hkc1xx_touch_config			(unsigned char state);

static int __devinit    hkc1xx_touch_probe			(struct platform_device *pdev);
static int __devexit    hkc1xx_touch_remove			(struct platform_device *pdev);

static int __init       hkc1xx_touch_init			(void);
static void __exit      hkc1xx_touch_exit			(void);

//[*]--------------------------------------------------------------------------------------------------[*]
static struct platform_driver hkc1xx_touch_platform_device_driver = {
		.probe          = hkc1xx_touch_probe,
		.remove         = hkc1xx_touch_remove,

#ifndef CONFIG_HAS_EARLYSUSPEND
		.suspend        = hkc1xx_touch_suspend,
		.resume         = hkc1xx_touch_resume,
#endif
		.driver		= {
			.owner	= THIS_MODULE,
			.name	= HKC1XX_TOUCH_DEVICE_NAME,
		},
};

//[*]--------------------------------------------------------------------------------------------------[*]
static struct platform_device hkc1xx_touch_platform_device = {
        .name           = HKC1XX_TOUCH_DEVICE_NAME,
        .id             = -1,
        .num_resources  = 0,
        .dev    = {
                .release	= hkc1xx_touch_release_device,
        },
};

//[*]--------------------------------------------------------------------------------------------------[*]
module_init(hkc1xx_touch_init);
module_exit(hkc1xx_touch_exit);

//[*]--------------------------------------------------------------------------------------------------[*]
MODULE_AUTHOR("HardKernel");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("7.0\" interface for HKC1XX-Dev Board");

//[*]--------------------------------------------------------------------------------------------------[*]
struct  timer_list		keysignal_timer;

static void hkc1xx_touch_keysignal_timer_start(void)
{
	del_timer_sync(&keysignal_timer);
	init_timer(&keysignal_timer);
	keysignal_timer.data 		= (unsigned long)&keysignal_timer;
	keysignal_timer.function 	= hkc1xx_touch_keysignal_timer_irq;
	keysignal_timer.expires = jiffies + PERIOD_50MS;
	add_timer(&keysignal_timer);
}

//[*]--------------------------------------------------------------------------------------------------[*]
static void	hkc1xx_touch_keysignal_timer_irq(unsigned long arg)
{
	gpio_set_value(TS_KEYSIG_PORT, 1);

	#if defined(DEBUG_HKC1XX_TOUCH_MSG)
		printk("TS_KEYSIG PORT HIGH(GPH2.2) : hkc1xx_touch_keysignal_timer_start!\n");
	#endif
}

//[*]--------------------------------------------------------------------------------------------------[*]
static void hkc1xx_touch_timer_start(void)
{
	init_timer(&hkc1xx_touch.penup_timer);
	hkc1xx_touch.penup_timer.data 		= (unsigned long)&hkc1xx_touch.penup_timer;
	hkc1xx_touch.penup_timer.function 	= hkc1xx_touch_timer_irq;
	
	switch(hkc1xx_touch.sampling_rate)	{
		default	:
			hkc1xx_touch.sampling_rate = 0;
			hkc1xx_touch.penup_timer.expires = jiffies + PERIOD_10MS;
			break;
		case	1:
			hkc1xx_touch.penup_timer.expires = jiffies + PERIOD_20MS;
			break;
		case	2:
			hkc1xx_touch.penup_timer.expires = jiffies + PERIOD_50MS;
			break;
	}

	add_timer(&hkc1xx_touch.penup_timer);
}

//[*]--------------------------------------------------------------------------------------------------[*]
static void	hkc1xx_touch_timer_irq(unsigned long arg)
{
	unsigned long	flags;
	
	local_irq_save(flags);	local_irq_disable();

	// Acc data read
	write_seqlock(&hkc1xx_touch.lock);

	if(!hkc1xx_touch_read_mem(get_object_address(GEN_MESSAGEPROCESSOR_T5, 0), &qt602240.pmessage[0], qt602240.max_message_length))	{
		if(qt602240.pmessage[0] > 1 && qt602240.pmessage[0] < 12)	{
			hkc1xx_touch_get_data(qt602240.pmessage[0] - 2);		hkc1xx_touch.status = TOUCH_PRESS;
		}
		else	{
			// Key Arrary Event
			if(qt602240.pmessage[0] == 0x0C)	{
				if((qt602240.pmessage[1] == 0x80) && (qt602240.pmessage[2] & 0x01))	{

					gpio_set_value(TS_KEYSIG_PORT, 0);		

					hkc1xx_touch_keysignal_timer_start();
					#if defined(DEBUG_HKC1XX_TOUCH_MSG)
						printk("TS_KEYSIG PORT LOW(GPH2.2) : hkc1xx_touch_keysignal_timer_start!\n");
					#endif
				}
				hkc1xx_touch_get_key_data((qt602240.pmessage[2]>>1) & 0x07);
			}
			else	{
				#if defined(DEBUG_HKC1XX_TOUCH_MSG)
					int	i;
					printk("report data = ");
					for(i=0; i<qt602240.max_message_length; i++) printk("0x%02X ", qt602240.pmessage[i]);		
					printk("\n");
				#endif
			}
		}
	}
	if((qt602240.pmessage[0] != 0xFF) || (!GET_INT_STATUS()))	hkc1xx_touch_timer_start();
	else														hkc1xx_touch.status = TOUCH_RELEASE;

	write_sequnlock(&hkc1xx_touch.lock);

	local_irq_restore(flags);
}

//[*]--------------------------------------------------------------------------------------------------[*]
static void	hkc1xx_touch_get_key_data(unsigned char in_data)
{
	unsigned char 	press_key, release_key, i;

	if(hkc1xx_touch.keydata != in_data)	{

		press_key	= (in_data ^ hkc1xx_touch.keydata) & in_data;
		release_key	= (in_data ^ hkc1xx_touch.keydata) & hkc1xx_touch.keydata;
		
		i = 0;
		while(press_key)	{
			if(press_key & 0x01)	input_report_key(hkc1xx_touch.driver, HKC1XX_TouchKeycode[i], 1);
			
			#if defined(DEBUG_HKC1XX_TOUCH_MSG)
				if(press_key & 0x01)	printk("PRESS KEY : %s", &HKC1XX_TouchKeyMapStr[i][0]);
			#endif
				
			i++;	press_key >>= 1;
		}

		i = 0;
		while(release_key)	{
			if(release_key & 0x01)	input_report_key(hkc1xx_touch.driver, HKC1XX_TouchKeycode[i], 0);

			#if defined(DEBUG_HKC1XX_TOUCH_MSG)
				if(release_key & 0x01)	printk("RELEASE KEY : %s", &HKC1XX_TouchKeyMapStr[i][0]);
			#endif

			i++;	release_key >>= 1;
		}
		
		hkc1xx_touch.keydata = in_data;

		#if	defined(DEBUG_HKC1XX_TOUCH_MSG)
			printk("Key Data = 0x%02X\n", hkc1xx_touch.keydata);
		#endif
	}
}

//[*]--------------------------------------------------------------------------------------------------[*]
static void hkc1xx_touch_get_data(unsigned int id)
{
	/* Check the touch is present on the screen */
	if (!(qt602240.pmessage[TS_STATUS] & QT602240_DETECT)) {
		if (qt602240.pmessage[TS_STATUS] & QT602240_RELEASE) {

			#if defined(DEBUG_HKC1XX_TOUCH_MSG)
				printk("ts id[%d] released\n", id);
			#endif

			qt602240.finger[id].status = QT602240_RELEASE;
			
			hkc1xx_touch_input_report(id);
		}
		return;
	}
	
	qt602240.finger[id].status = (qt602240.pmessage[TS_STATUS] & QT602240_MOVE) ? QT602240_MOVE : QT602240_PRESS;
	qt602240.finger[id].x = (qt602240.pmessage[TS_X_UPPER] <<2) + ((qt602240.pmessage[TS_XY_LOWER] & 0xC0) >>6);
	qt602240.finger[id].y = (qt602240.pmessage[TS_Y_UPPER] <<2) + ((qt602240.pmessage[TS_XY_LOWER] & 0x0C) >>2);
	qt602240.finger[id].area = (qt602240.pmessage[TS_AREA]);

	#if defined(DEBUG_HKC1XX_TOUCH_MSG)
		printk("[%d] %s x: %d, y: %d, area: %d\n", 	id, 
													qt602240.pmessage[TS_STATUS] & QT602240_MOVE ? "moved" : "pressed",
													qt602240.finger[id].x,
													qt602240.finger[id].y,
													qt602240.finger[id].area);
	#endif
														
	hkc1xx_touch_input_report(id);
}

//------------------------------------------------------------------------------------------------------------------------
static	void	hkc1xx_touch_input_report(unsigned int id)
{
	int finger_num, cnt;

	for (cnt = 0, finger_num = 0; cnt < TS_MAX_FINGER; cnt++) {
		if (!qt602240.finger[cnt].status)	continue;

		input_report_abs(hkc1xx_touch.driver, ABS_MT_TOUCH_MAJOR, 
											qt602240.finger[cnt].status != QT602240_RELEASE ? qt602240.finger[cnt].area : 0);
		input_report_abs(hkc1xx_touch.driver, ABS_MT_WIDTH_MAJOR,
											qt602240.finger[cnt].status != QT602240_RELEASE ? qt602240.finger[cnt].area * 10: 0);
		input_report_abs(hkc1xx_touch.driver, ABS_MT_POSITION_X, qt602240.finger[cnt].x);
		input_report_abs(hkc1xx_touch.driver, ABS_MT_POSITION_Y, qt602240.finger[cnt].y);
		input_mt_sync(hkc1xx_touch.driver);

		if (qt602240.finger[cnt].status == QT602240_RELEASE)
			qt602240.finger[cnt].status = 0;
		else
			finger_num++;
	}

	input_report_key(hkc1xx_touch.driver, BTN_TOUCH, finger_num > 0);

	if (qt602240.finger[id].status != QT602240_RELEASE) {
		input_report_abs(hkc1xx_touch.driver, ABS_X, qt602240.finger[id].x);
		input_report_abs(hkc1xx_touch.driver, ABS_Y, qt602240.finger[id].y);
	}

	input_sync(hkc1xx_touch.driver);
}

//------------------------------------------------------------------------------------------------------------------------
irqreturn_t	hkc1xx_touch_irq(int irq, void *dev_id)
{
	unsigned long	flags;

	local_irq_save(flags);	local_irq_disable();

	if(!hkc1xx_touch.status)	{
		del_timer_sync(&hkc1xx_touch.penup_timer);
		hkc1xx_touch_timer_start();
	}

	local_irq_restore(flags);

	return	IRQ_HANDLED;
}

//[*]--------------------------------------------------------------------------------------------------[*]
static int	hkc1xx_touch_open	(struct input_dev *dev)
{
	#if	defined(DEBUG_HKC1XX_TOUCH_MSG)
		printk("%s\n", __FUNCTION__);
	#endif
	
	return	0;
}

//[*]--------------------------------------------------------------------------------------------------[*]
static void	hkc1xx_touch_close	(struct input_dev *dev)
{
	#if	defined(DEBUG_HKC1XX_TOUCH_MSG)
		printk("%s\n", __FUNCTION__);
	#endif
}

//[*]--------------------------------------------------------------------------------------------------[*]
static void	hkc1xx_touch_release_device	(struct device *dev)
{
	#if	defined(DEBUG_HKC1XX_TOUCH_MSG)
		printk("%s\n", __FUNCTION__);
	#endif
}

//[*]--------------------------------------------------------------------------------------------------[*]
//[*]--------------------------------------------------------------------------------------------------[*]
#ifdef	CONFIG_HAS_EARLYSUSPEND
static void		hkc1xx_touch_late_resume	(struct early_suspend *h)
#else
static 	int		hkc1xx_touch_resume			(struct platform_device *dev)
#endif
{
	#if	defined(DEBUG_HKC1XX_TOUCH_PM_MSG)
		printk("%s\n", __FUNCTION__);
	#endif

	hkc1xx_touch_config(TOUCH_STATE_RESUME);

	// interrupt enable
	enable_irq(HKC1XX_TOUCH_IRQ);

#ifndef	CONFIG_HAS_EARLYSUSPEND
	return	0;
#endif	
}
//[*]--------------------------------------------------------------------------------------------------[*]
#ifdef	CONFIG_HAS_EARLYSUSPEND
static void		hkc1xx_touch_early_suspend	(struct early_suspend *h)
#else
static 	int		hkc1xx_touch_suspend		(struct platform_device *dev, pm_message_t state)
#endif
{
	#if	defined(DEBUG_HKC1XX_TOUCH_PM_MSG)
		printk("%s\n", __FUNCTION__);
	#endif

	del_timer_sync(&keysignal_timer);
	del_timer_sync(&hkc1xx_touch.penup_timer);

	// interrupt disable
	disable_irq(HKC1XX_TOUCH_IRQ);

#ifndef	CONFIG_HAS_EARLYSUSPEND
	return	0;
#endif	
}

//[*]--------------------------------------------------------------------------------------------------[*]
static void		hkc1xx_touch_reset(void)
{
}

//[*]--------------------------------------------------------------------------------------------------[*]
static void 	hkc1xx_touch_config(unsigned char state)
{
	hkc1xx_touch_reset();
	
	hkc1xx_touch_port_init();

	if(state == TOUCH_STATE_BOOT)	{
		// Interrup Line Pull-up
		if(gpio_request(TS_IRQ_PORT, TS_IRQ_STR))	{
			printk("%s(%s) : request port error!\n", __FUNCTION__, TS_IRQ_STR);
		}
		s3c_gpio_setpull(TS_IRQ_PORT, S3C_GPIO_PULL_UP);
		
		// set key port
		if(gpio_request(TS_KEYSIG_PORT, TS_KEYSIG_STR))	{
			printk("%s(%s) : request port error!\n", __FUNCTION__, TS_KEYSIG_STR);
		}
	    gpio_direction_output(TS_KEYSIG_PORT, 1);	s3c_gpio_setpull(TS_KEYSIG_PORT, S3C_GPIO_PULL_NONE);
		gpio_set_value(TS_KEYSIG_PORT, 1);

		// Touch screen init
		qt602240_init();
	
		if(!request_irq(HKC1XX_TOUCH_IRQ, hkc1xx_touch_irq, IRQF_DISABLED, "HKC1XX-Touch IRQ", (void *)&hkc1xx_touch))	{
			printk("HKC1XX TOUCH request_irq = %d\r\n" , HKC1XX_TOUCH_IRQ);
		}
		else	{
			printk("HKC1XX TOUCH request_irq = %d error!! \r\n", HKC1XX_TOUCH_IRQ);
		}
		set_irq_type(HKC1XX_TOUCH_IRQ, IRQ_TYPE_EDGE_FALLING);
	
		// seqlock init
		seqlock_init(&hkc1xx_touch.lock);		hkc1xx_touch.seq = 0;
	}
	else		qt602240_set_config();

	hkc1xx_touch.status = TOUCH_RELEASE;
}

//[*]--------------------------------------------------------------------------------------------------[*]
static int __devinit    hkc1xx_touch_probe	(struct platform_device *pdev)
{
    int 	key, code, rc;

	// struct init
	memset(&hkc1xx_touch, 0x00, sizeof(hkc1xx_touch_t));
	
	// create sys_fs
	if((rc = hkc1xx_touch_sysfs_create(pdev)))	{
		printk("%s : sysfs_create_group fail!!\n", __FUNCTION__);
		return	rc;
	}
	
	hkc1xx_touch.driver = input_allocate_device();

    if(!(hkc1xx_touch.driver))	{
		printk("ERROR! : %s cdev_alloc() error!!! no memory!!\n", __FUNCTION__);
		hkc1xx_touch_sysfs_remove(pdev);
		return -ENOMEM;
    }

	hkc1xx_touch.driver->name 	= HKC1XX_TOUCH_DEVICE_NAME;
	hkc1xx_touch.driver->phys 	= "hkc1xx_touch/input1";
    hkc1xx_touch.driver->open 	= hkc1xx_touch_open;
    hkc1xx_touch.driver->close	= hkc1xx_touch_close;

	hkc1xx_touch.driver->id.bustype	= BUS_HOST;
	hkc1xx_touch.driver->id.vendor 	= 0x16B4;
	hkc1xx_touch.driver->id.product	= 0x0702;
	hkc1xx_touch.driver->id.version	= 0x0001;

	hkc1xx_touch.driver->evbit[0]  = BIT_MASK(EV_KEY) | BIT_MASK(EV_ABS);

	set_bit(BTN_TOUCH & KEY_MAX, hkc1xx_touch.driver->keybit);

	// Touch Key Event
	for(key = 0; key < MAX_KEYCODE_CNT; key++){
		code = HKC1XX_TouchKeycode[key];
		if(code<=0)
			continue;
		set_bit(code & KEY_MAX, hkc1xx_touch.driver->keybit);
	}

	/* For single touch */
	input_set_abs_params(hkc1xx_touch.driver, ABS_X, TS_ABS_MIN_X, TS_ABS_MAX_X, 0, 0);
	input_set_abs_params(hkc1xx_touch.driver, ABS_Y, TS_ABS_MIN_Y, TS_ABS_MAX_Y, 0, 0);

	/* multi touch */
	input_set_abs_params(hkc1xx_touch.driver, ABS_MT_POSITION_X, TS_ABS_MIN_X, TS_ABS_MAX_X, 0, 0);
	input_set_abs_params(hkc1xx_touch.driver, ABS_MT_POSITION_Y, TS_ABS_MIN_Y, TS_ABS_MAX_Y, 0, 0);
	input_set_abs_params(hkc1xx_touch.driver, ABS_MT_TOUCH_MAJOR, 0, TS_MAX_AREA, 0, 0);
	input_set_abs_params(hkc1xx_touch.driver, ABS_MT_WIDTH_MAJOR, 0, TS_MAX_AREA, 0, 0);
	
	if(input_register_device(hkc1xx_touch.driver))	{
		printk("HKC1XX TOUCH input register device fail!!\n");

		hkc1xx_touch_sysfs_remove(pdev);
		input_free_device(hkc1xx_touch.driver);		return	-ENODEV;
	}

#ifdef CONFIG_HAS_EARLYSUSPEND
	hkc1xx_touch.power.suspend 	= hkc1xx_touch_early_suspend;
	hkc1xx_touch.power.resume 	= hkc1xx_touch_late_resume;
	hkc1xx_touch.power.level 	= EARLY_SUSPEND_LEVEL_DISABLE_FB-1;
	//if, is in USER_SLEEP status and no active auto expiring wake lock
	//if (has_wake_lock(WAKE_LOCK_SUSPEND) == 0 && get_suspend_state() == PM_SUSPEND_ON)
	register_early_suspend(&hkc1xx_touch.power);
#endif

	hkc1xx_touch_config(TOUCH_STATE_BOOT);

	printk("--------------------------------------------------------\n");
	printk("HardKernel : ODROID-SMD Multi Touch driver initialized!! Ver 1.0\n");
	printk("--------------------------------------------------------\n");

	return	0;
}

//[*]--------------------------------------------------------------------------------------------------[*]
static int __devexit    hkc1xx_touch_remove	(struct platform_device *pdev)
{
	#if	defined(DEBUG_HKC1XX_TOUCH_MSG)
		printk("%s\n", __FUNCTION__);
	#endif
	
	del_timer_sync(&keysignal_timer);	del_timer_sync(&hkc1xx_touch.penup_timer);
	
	gpio_free(TS_IRQ_PORT);				gpio_free(TS_KEYSIG_PORT);
	
	free_irq(HKC1XX_TOUCH_IRQ, (void *)&hkc1xx_touch); 

	hkc1xx_touch_sysfs_remove(pdev);

	input_unregister_device(hkc1xx_touch.driver);

	return  0;
}

//[*]--------------------------------------------------------------------------------------------------[*]
static int __init	hkc1xx_touch_init	(void)
{
	int ret = platform_driver_register(&hkc1xx_touch_platform_device_driver);
	
	#if	defined(DEBUG_HKC1XX_TOUCH_MSG)
		printk("%s\n", __FUNCTION__);
	#endif
	
	if(!ret)        {
		ret = platform_device_register(&hkc1xx_touch_platform_device);
		
		#if	defined(DEBUG_HKC1XX_TOUCH_MSG)
			printk("platform_driver_register %d \n", ret);
		#endif
		
		if(ret)	platform_driver_unregister(&hkc1xx_touch_platform_device_driver);
	}
	return ret;
}

//[*]--------------------------------------------------------------------------------------------------[*]
static void __exit	hkc1xx_touch_exit	(void)
{
	#if	defined(DEBUG_HKC1XX_TOUCH_MSG)
		printk("%s\n",__FUNCTION__);
	#endif
	
	platform_device_unregister(&hkc1xx_touch_platform_device);
	platform_driver_unregister(&hkc1xx_touch_platform_device_driver);
}

//[*]--------------------------------------------------------------------------------------------------[*]
//[*]--------------------------------------------------------------------------------------------------[*]
