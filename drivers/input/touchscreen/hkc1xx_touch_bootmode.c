//[*]----------------------------------------------------------------------------------------------[*]
//
//
// 
//  HardKernel-C1XX gpio i2c driver (charles.park)
//  2009.07.22
// 
//
//[*]----------------------------------------------------------------------------------------------[*]
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/device.h>
#include <linux/input.h>
#include <linux/fs.h>

#include <mach/irqs.h>
#include <asm/system.h>

#include <linux/delay.h>
#include <mach/regs-gpio.h>
#include <mach/gpio-bank.h>

//[*]----------------------------------------------------------------------------------------------[*]
#include "hkc1xx_touch_gpio_i2c.h"
#include "hkc1xx_touch.h"

//[*]----------------------------------------------------------------------------------------------[*]
// Define value
//[*]----------------------------------------------------------------------------------------------[*]
#define	FLASH_ERASE_SIZE		0x200	// 512 word
#define	FLASH_START_ADDRESS		0x1000

#define	CMD_FLASH_WR_PROTECT	0x03
	#define	DATA_PROTECT_ERASE	0x5A
#define	CMD_FLASH_ERASE			0xE0
#define	CMD_FLASH_ACCESS		0x00
#define	CMD_FLASH_ADDRESS		0x01
#define	CMD_FLASH_CHECKSUM		0x30
#define	CMD_FLASH_RESET			0xB0
	#define	DATA_RESET			0x00
	
#define	INFO_VERSION_ADDR		0x1100
#define	ADDRESS_OF_FW_VER		0x200
#define	ADDRESS_OF_FW_REV		0x201

#define	FLASH_WRITE_SIZE		32		// 16 word write

//[*]----------------------------------------------------------------------------------------------[*]
//[*]----------------------------------------------------------------------------------------------[*]
//	static function prototype
//[*]----------------------------------------------------------------------------------------------[*]
static	int		hkc1xx_touch_busy_check			(void);
static	int		hkc1xx_touch_checksum_verify	(void);
static	int		hkc1xx_touch_flash_erase		(void);
static 	int		hkc1xx_touch_fw_write			(void);
static 	int		hkc1xx_touch_check_version		(void);

//[*]----------------------------------------------------------------------------------------------[*]
//	extern function prototype
//[*]----------------------------------------------------------------------------------------------[*]
int				hkc1xx_touch_fw_upgrade			(void);
int				hkc1xx_touch_mode_change		(unsigned char mode);
void			hkc1xx_touch_get_fw_version		(void);
int				hkc1xx_touch_fw_upgrade_check	(void);

//[*]----------------------------------------------------------------------------------------------[*]
//[*]----------------------------------------------------------------------------------------------[*]
static	int		hkc1xx_touch_busy_check			(void)
{
	int	retry = 10000;	// wait 100ms
	
	while(retry)	{
		if(GET_INT_STATUS())	return	0;
		udelay(10);				retry--;
	}
	return	-1;
}

//[*]----------------------------------------------------------------------------------------------[*]
static	int		hkc1xx_touch_checksum_verify	(void)
{
	return	0;
}

//[*]----------------------------------------------------------------------------------------------[*]
static	int		hkc1xx_touch_flash_erase		(void)
{
	unsigned char	buffer[4];
	unsigned short	cnt, addr = FLASH_START_ADDRESS;
	unsigned int	erase_size = (hkc1xx_touch.fw_size / FLASH_ERASE_SIZE) + 1;
	
   	// write protect erase
   	buffer[0] = DATA_PROTECT_ERASE;	   	buffer[1] = DATA_PROTECT_ERASE;
   	
	if(!hkc1xx_touch_busy_check())	hkc1xx_touch_bootmode_write(CMD_FLASH_WR_PROTECT, &buffer[0], 2);
	else							return	-1;
   	
	printk("FLASH Protect ERASE OK!\n");	

   	// flash erase (size = fw_size + 1)
	printk("FLASH ERASE! .");	
   	
   	for(cnt = 0; cnt < erase_size; cnt++)	{
	   	buffer[0] = (addr >> 8) & 0xFF;	   	buffer[1] = (addr     ) & 0xFF;
	   	buffer[2] = (addr >> 8) & 0xFF;	   	buffer[3] = (addr     ) & 0xFF;

		if(!hkc1xx_touch_busy_check())	{
			hkc1xx_touch_bootmode_write(CMD_FLASH_ERASE, &buffer[0], 4);
			addr += FLASH_ERASE_SIZE;
			printk(".");	
		}
		else	{
			printk("%s(%d) : Error!\n", __FUNCTION__, __LINE__);	
			return	-1;
		}
   	}
	printk("OK!.\n");	
   	
   	return	0;
}

//[*]----------------------------------------------------------------------------------------------[*]
static 	int		hkc1xx_touch_fw_write			(void)
{
	unsigned char	buffer[4];
	unsigned short	addr = FLASH_START_ADDRESS + 0x10;	// 2'nd start
	unsigned int	cnt, wp, write_size = (hkc1xx_touch.fw_size / FLASH_WRITE_SIZE) + 1; 

   	buffer[0] = (addr >> 8) & 0xFF;	   	buffer[1] = (addr     ) & 0xFF;
	if(!hkc1xx_touch_busy_check())	hkc1xx_touch_bootmode_write(CMD_FLASH_ADDRESS, &buffer[0], 2);
	else							return	-1;

	printk("FLASH WRITE! .");	
   	for(cnt = 1, wp = FLASH_WRITE_SIZE; cnt < write_size; cnt++, wp += FLASH_WRITE_SIZE)	{
		if(!hkc1xx_touch_busy_check())	hkc1xx_touch_bootmode_write(CMD_FLASH_ACCESS, &hkc1xx_touch.fw[wp], FLASH_WRITE_SIZE);
		else							return	-1;
		printk(".");	
   	}
   	
   	// 1'st write
   	addr = FLASH_START_ADDRESS;
   	buffer[0] = (addr >> 8) & 0xFF;	   	buffer[1] = (addr     ) & 0xFF;
	if(!hkc1xx_touch_busy_check())	hkc1xx_touch_bootmode_write(CMD_FLASH_ADDRESS, &buffer[0], 2);
	else							return	-1;
	printk(".");	

	// first 16 word write
	if(!hkc1xx_touch_busy_check())	hkc1xx_touch_bootmode_write(CMD_FLASH_ACCESS, &hkc1xx_touch.fw[0], FLASH_WRITE_SIZE);
	else							return	-1;
	printk(".");	

	printk("OK!.\n");	
		
	return	0;
}

//[*]----------------------------------------------------------------------------------------------[*]
static 	int		hkc1xx_touch_check_version		(void)
{
	// EVT Firmware
	if((hkc1xx_touch.fw_ver != 0x01) || (hkc1xx_touch.fw_rev < hkc1xx_touch.fw[ADDRESS_OF_FW_REV]))	goto upgrade;
		
	printk("\nAlready Firmware Upgrade : [ Current Ver : 0x%02X, Rev : 0x%02X ], [ New Ver : 0x%02X, Rev : 0x%02X ]\n", 
		hkc1xx_touch.fw_ver, hkc1xx_touch.fw_rev, hkc1xx_touch.fw[ADDRESS_OF_FW_VER], hkc1xx_touch.fw[ADDRESS_OF_FW_REV]);
	
	return	1;		// same firmware

upgrade:
	printk("\nFirmware Upgrade : [ Current Ver : 0x%02X, Rev : 0x%02X ] ==> [ New Ver : 0x%02X, Rev : 0x%02X ]\n", 
		hkc1xx_touch.fw_ver, hkc1xx_touch.fw_rev, hkc1xx_touch.fw[ADDRESS_OF_FW_VER], hkc1xx_touch.fw[ADDRESS_OF_FW_REV]);
	hkc1xx_touch.fw_ver = hkc1xx_touch.fw[ADDRESS_OF_FW_VER];
	hkc1xx_touch.fw_rev = hkc1xx_touch.fw[ADDRESS_OF_FW_REV];
	return	0;	// new firmware
}

//[*]----------------------------------------------------------------------------------------------[*]
int				hkc1xx_touch_mode_change		(unsigned char mode)
{
	unsigned char	cur_mode;
	
	if(!hkc1xx_touch_bootmode_read(&hkc1xx_touch.rd[0], 2))		cur_mode = TOUCH_MODE_BOOT;
	else														cur_mode = TOUCH_MODE_NORMAL;
		
	if(cur_mode == mode)	return	0;

	mdelay(50);		
	hkc1xx_touch.rd[0] = 0x00;	hkc1xx_touch.rd[1] = 0x00;		

	if(mode)	{	// CHANGE to BOOT MODE

		hkc1xx_touch_write(TS_RESET, &hkc1xx_touch.rd[0], 2);	mdelay(50);

		if(!hkc1xx_touch_bootmode_read(&hkc1xx_touch.rd[0], 2))	{
			printk("Currnet Touch Mode ==> BOOT MODE\n");		return	0;
		}
	}
	else	{		// NORMAL MODE
		hkc1xx_touch_bootmode_write(TS_BOOTMODE_RESET, &hkc1xx_touch.rd[0], 2);		mdelay(50);
		hkc1xx_touch_bootmode_read(&hkc1xx_touch.rd[0], 2);							mdelay(50);
		
		if(!hkc1xx_touch_read(&hkc1xx_touch.rd[0], 2))	{
			printk("Currnet Touch Mode ==> NORMAL MODE\n");		return	0;
		}
	}

	printk("Currnet Touch Mode ==> Unknown MODE???\n");
	return	-1;
}
//[*]----------------------------------------------------------------------------------------------[*]
void			hkc1xx_touch_get_fw_version		(void)
{
	unsigned char	buffer[2];
	
	// change bootmode
	hkc1xx_touch_mode_change(TOUCH_MODE_BOOT);
	
	hkc1xx_touch.fw_ver  = 0xEE;		hkc1xx_touch.fw_rev = 0xEE;

	buffer[0] = (INFO_VERSION_ADDR >> 8) & 0xFF;	buffer[1] = (INFO_VERSION_ADDR     ) & 0xFF;
	
	// set info address
	if(!hkc1xx_touch_busy_check())	hkc1xx_touch_bootmode_write	(CMD_FLASH_ADDRESS, &buffer[0], 2);
	else							goto out;

	// change to data access mode
	if(!hkc1xx_touch_busy_check())	hkc1xx_touch_bootmode_write(CMD_FLASH_ACCESS, &buffer[0], 0);
	else							goto out;

	// get info data		
	if(!hkc1xx_touch_busy_check())	hkc1xx_touch_bootmode_read	(&buffer[0], 2);
	else							goto out;

	hkc1xx_touch.fw_ver  = buffer[0];		hkc1xx_touch.fw_rev = buffer[1];
		
	printk("\nTouch Firmware Info [ Ver : 0x%02X, Rev : 0x%02X ]\n", hkc1xx_touch.fw_ver, hkc1xx_touch.fw_rev);

out:	
	// change normal mode
	hkc1xx_touch_mode_change(TOUCH_MODE_NORMAL);
}

//[*]----------------------------------------------------------------------------------------------[*]
// return 0 => upgrade, return 1 => no upgrade (same f/w), return -1 => error
//[*]----------------------------------------------------------------------------------------------[*]
int				hkc1xx_touch_fw_upgrade			(void)	
{
	int 	ret = 0;

	// change bootmode
	hkc1xx_touch_mode_change(TOUCH_MODE_BOOT);
	
	// same firmware or error then return
	if((ret = hkc1xx_touch_check_version	()) != 0 )				goto out;
	
	// error then return
	if((ret = hkc1xx_touch_flash_erase		()) != 0 )				goto out;

	// error then return
	if((ret = hkc1xx_touch_fw_write			()) != 0 )				goto out;

	// error then return
	if((ret = hkc1xx_touch_checksum_verify	()) != 0 )				goto out;

out:
	// change normal mode
	hkc1xx_touch_mode_change(TOUCH_MODE_NORMAL);

	// f/w upgrade value clear
	hkc1xx_touch.fw_size = 0;	
	
	memset(hkc1xx_touch.fw, 0xFF, MAX_FW_SIZE);
	
	return	ret;	// upgrade sucess
}

//[*]----------------------------------------------------------------------------------------------[*]
//[*]----------------------------------------------------------------------------------------------[*]
