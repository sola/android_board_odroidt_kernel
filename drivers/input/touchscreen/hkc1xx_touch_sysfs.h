//[*]--------------------------------------------------------------------------------------------------[*]
/*
 *	
 * S5PC100 _HKC1XX_TOUCH_SYSFS_H_ Header file(charles.park) 
 *
 */
//[*]--------------------------------------------------------------------------------------------------[*]

#ifndef	_HKC1XX_TOUCH_SYSFS_H_
#define	_HKC1XX_TOUCH_SYSFS_H_

extern	int		hkc1xx_touch_sysfs_create		(struct platform_device *pdev);
extern	void	hkc1xx_touch_sysfs_remove		(struct platform_device *pdev);
#if defined(CONFIG_TOUCHSCREEN_ODROID_MT_7) || defined(CONFIG_TOUCHSCREEN_ODROID_MT_7_P)		// Odroid7 touchscreen
	extern	void	hkc1xx_touch_fw_upgrade_check	(void);
#endif

//[*]--------------------------------------------------------------------------------------------------[*]
#endif	// _HKC1XX_TOUCH_SYSFS_H_
//[*]--------------------------------------------------------------------------------------------------[*]

