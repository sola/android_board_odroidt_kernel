//[*]--------------------------------------------------------------------------------------------------[*]
//
//
// 
//  HKC1XX Board : Touch Sensor Interface driver (charles.park)
//  2009.10.09
// 
//
//[*]--------------------------------------------------------------------------------------------------[*]
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/device.h>
#include <linux/input.h>
#include <linux/fs.h>

#include <mach/irqs.h>
#include <asm/system.h>

#include <linux/delay.h>
#include <mach/regs-gpio.h>
#include <mach/gpio-bank.h>

//[*]----------------------------------------------------------------------------------------------[*]
#include "hkc1xx_touch_gpio_i2c.h"
#include "hkc1xx_touch.h"

#if defined(CONFIG_TOUCHSCREEN_ODROID_MT_7) || defined(CONFIG_TOUCHSCREEN_ODROID_MT_7_P)
	#include "hkc1xx_touch_bootmode.h"
	#include "hkc1xx_touch_default_fw.h"
#endif
//[*]--------------------------------------------------------------------------------------------------[*]
//
//   function prototype define
//
//[*]--------------------------------------------------------------------------------------------------[*]
int		hkc1xx_touch_sysfs_create		(struct platform_device *pdev);
void	hkc1xx_touch_sysfs_remove		(struct platform_device *pdev);	
		
//[*]--------------------------------------------------------------------------------------------------[*]
//
//   sysfs function prototype define
//
//[*]--------------------------------------------------------------------------------------------------[*]
//   screen hold control (on -> hold, off -> normal mode)
//[*]--------------------------------------------------------------------------------------------------[*]
static	ssize_t show_hold_state			(struct device *dev, struct device_attribute *attr, char *buf);
static	ssize_t set_hold_state			(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
static	DEVICE_ATTR(hold_state, S_IRWXUGO, show_hold_state, set_hold_state);

//[*]--------------------------------------------------------------------------------------------------[*]
//   touch sampling rate control (5, 10, 20 : unit msec)
//[*]--------------------------------------------------------------------------------------------------[*]
static	ssize_t show_sampling_rate		(struct device *dev, struct device_attribute *attr, char *buf);
static	ssize_t set_sampling_rate		(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
static	DEVICE_ATTR(sampling_rate, S_IRWXUGO, show_sampling_rate, set_sampling_rate);

//[*]--------------------------------------------------------------------------------------------------[*]
// 	touch threshold control (range 0 - 10) : default 3
//[*]--------------------------------------------------------------------------------------------------[*]
#define	THRESHOLD_MAX	10

//[*]--------------------------------------------------------------------------------------------------[*]
static 	ssize_t show_threshold_x		(struct device *dev, struct device_attribute *attr, char *buf);
static 	ssize_t set_threshold_x			(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
static	DEVICE_ATTR(threshold_x, S_IRWXUGO, show_threshold_x, set_threshold_x);

static 	ssize_t show_threshold_y		(struct device *dev, struct device_attribute *attr, char *buf);
static 	ssize_t set_threshold_y			(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
static	DEVICE_ATTR(threshold_y, S_IRWXUGO, show_threshold_y, set_threshold_y);

#if defined(CONFIG_TOUCHSCREEN_KETI)
	static 	ssize_t show_threshold_z		(struct device *dev, struct device_attribute *attr, char *buf);
	static 	ssize_t set_threshold_z			(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
	static	DEVICE_ATTR(threshold_z, S_IRWXUGO, show_threshold_z, set_threshold_z);

	static 	ssize_t show_debug		(struct device *dev, struct device_attribute *attr, char *buf);
	static 	ssize_t set_debug		(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
	static	DEVICE_ATTR(debug, S_IRWXUGO, show_debug, set_debug);
#endif

//[*]--------------------------------------------------------------------------------------------------[*]
//   touch calibration
//[*]--------------------------------------------------------------------------------------------------[*]
#if (defined(CONFIG_TOUCHSCREEN_ODROID_MT_T)||defined(CONFIG_TOUCHSCREEN_X10)||defined(CONFIG_TOUCHSCREEN_ODROID_MT_7)||defined(CONFIG_TOUCHSCREEN_ODROID_MT_7_P)) 
	static	ssize_t set_ts_cal		(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
	static	DEVICE_ATTR(ts_cal, S_IWUGO, NULL, set_ts_cal);

	#if defined(CONFIG_TOUCHSCREEN_ODROID_MT_7) || defined(CONFIG_TOUCHSCREEN_ODROID_MT_7_P)
		static	ssize_t set_ts_sense	(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
		static 	ssize_t show_ts_sense		(struct device *dev, struct device_attribute *attr, char *buf);
	
		static	DEVICE_ATTR(ts_sense, S_IRWXUGO, show_ts_sense, set_ts_sense);

		static	ssize_t set_ts_enable	(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
		static 	ssize_t show_ts_enable	(struct device *dev, struct device_attribute *attr, char *buf);
	
		static	DEVICE_ATTR(ts_enable, S_IRWXUGO, show_ts_enable, set_ts_enable);

		static	ssize_t set_ts_key_enable	(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
		static 	ssize_t show_ts_key_enable	(struct device *dev, struct device_attribute *attr, char *buf);
	
		static	DEVICE_ATTR(ts_key_enable, S_IRWXUGO, show_ts_key_enable, set_ts_key_enable);

		static	ssize_t set_ts_upgrade		(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
		static 	ssize_t show_ts_upgrade		(struct device *dev, struct device_attribute *attr, char *buf);
	
		static	DEVICE_ATTR(ts_upgrade, S_IRWXUGO, show_ts_upgrade, set_ts_upgrade);

		static	ssize_t set_ts_upgrade_data	(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
		static 	ssize_t show_ts_upgrade_data(struct device *dev, struct device_attribute *attr, char *buf);
	
		static	DEVICE_ATTR(ts_upgrade_data, S_IRWXUGO, show_ts_upgrade_data, set_ts_upgrade_data);

		static	ssize_t set_ts_firmware_info(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
		static 	ssize_t show_ts_firmware_info(struct device *dev, struct device_attribute *attr, char *buf);
	
		static	DEVICE_ATTR(ts_firmware_info, S_IRWXUGO, show_ts_firmware_info, set_ts_firmware_info);

		unsigned char	upgrade_status = 0;
		struct  timer_list		upgrade_timer;

	#endif
#endif

//[*]--------------------------------------------------------------------------------------------------[*]
//[*]--------------------------------------------------------------------------------------------------[*]
static struct attribute *hkc1xx_touch_sysfs_entries[] = {
	&dev_attr_hold_state.attr,
	&dev_attr_sampling_rate.attr,
	&dev_attr_threshold_x.attr,
	&dev_attr_threshold_y.attr,

#if defined(CONFIG_TOUCHSCREEN_KETI)
	&dev_attr_threshold_z.attr,
	&dev_attr_debug.attr,
#endif

#if (defined(CONFIG_TOUCHSCREEN_ODROID_MT_T)||defined(CONFIG_TOUCHSCREEN_X10)||defined(CONFIG_TOUCHSCREEN_ODROID_MT_7)||defined(CONFIG_TOUCHSCREEN_ODROID_MT_7_P))
	&dev_attr_ts_cal.attr,
	#if defined(CONFIG_TOUCHSCREEN_ODROID_MT_7) || defined(CONFIG_TOUCHSCREEN_ODROID_MT_7_P)
		&dev_attr_ts_sense.attr,
		&dev_attr_ts_enable.attr,
		&dev_attr_ts_key_enable.attr,
		&dev_attr_ts_upgrade.attr,
		&dev_attr_ts_upgrade_data.attr,
		&dev_attr_ts_firmware_info.attr,
	#endif
#endif
	NULL
};

static struct attribute_group hkc1xx_touch_attr_group = {
	.name   = NULL,
	.attrs  = hkc1xx_touch_sysfs_entries,
};

//[*]--------------------------------------------------------------------------------------------------[*]
//[*]--------------------------------------------------------------------------------------------------[*]
static 	ssize_t show_hold_state			(struct device *dev, struct device_attribute *attr, char *buf)
{
	if(hkc1xx_touch.hold_status)		return	sprintf(buf, "on\n");
	else								return	sprintf(buf, "off\n");
}

//[*]--------------------------------------------------------------------------------------------------[*]
static 	ssize_t set_hold_state			(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    unsigned long flags;

    local_irq_save(flags);

	if(!strcmp(buf, "on\n")) 
		hkc1xx_touch.hold_status = 1;
	else {
#if (defined(CONFIG_TOUCHSCREEN_ODROID_MT_T)||defined(CONFIG_TOUCHSCREEN_X10))
		unsigned char 	wdata;
	
		// Touchscreen Active mode
		wdata = 0x00;
		hkc1xx_touch_write(MODULE_POWERMODE, &wdata, 1);
		mdelay(10);

		// INT_mode : disable interrupt
		wdata = 0x00;
		hkc1xx_touch_write(MODULE_INTMODE, &wdata, 1);
	
		// touch calibration
		wdata =0x03;
		hkc1xx_touch_write(MODULE_CALIBRATION, &wdata, 1);	// set mode
		
		mdelay(500);

		// INT_mode : disable interrupt, low-active, finger moving
		wdata = 0x01;
		hkc1xx_touch_write(MODULE_INTMODE, &wdata, 1);
		mdelay(10);

		// INT_mode : enable interrupt, low-active, finger moving
		wdata = 0x09;
		hkc1xx_touch_write(MODULE_INTMODE, &wdata, 1);
		mdelay(10);
#endif
		hkc1xx_touch.hold_status = 0;
	}

    local_irq_restore(flags);

    return count;
}

//[*]--------------------------------------------------------------------------------------------------[*]
static 	ssize_t show_sampling_rate		(struct device *dev, struct device_attribute *attr, char *buf)
{
	switch(hkc1xx_touch.sampling_rate)	{
		default	:
			hkc1xx_touch.sampling_rate = 0;
		case	0:
			return	sprintf(buf, "10 msec\n");
		case	1:
			return	sprintf(buf, "20 msec\n");
		case	2:
			return	sprintf(buf, "50 msec\n");
	}
}

//[*]--------------------------------------------------------------------------------------------------[*]
static 	ssize_t set_sampling_rate		(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    unsigned long 	flags;
    unsigned int	val;

    if(!(sscanf(buf, "%u\n", &val)))	return	-EINVAL;

    local_irq_save(flags);

    if		(val > 20)		hkc1xx_touch.sampling_rate = 2;
    else if	(val > 10)		hkc1xx_touch.sampling_rate = 1;
    else					hkc1xx_touch.sampling_rate = 0;

    local_irq_restore(flags);

    return count;
}

//[*]--------------------------------------------------------------------------------------------------[*]
static 	ssize_t show_threshold_x		(struct device *dev, struct device_attribute *attr, char *buf)
{
	if(hkc1xx_touch.threshold_x > THRESHOLD_MAX)	hkc1xx_touch.threshold_x = THRESHOLD_MAX;
		
	return	sprintf(buf, "%d\n", hkc1xx_touch.threshold_x);
}

//[*]--------------------------------------------------------------------------------------------------[*]
static 	ssize_t set_threshold_x			(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    unsigned long 	flags;
    unsigned int	val;

    if(!(sscanf(buf, "%u\n", &val)))	return	-EINVAL;

    local_irq_save(flags);

	if(val < 0)				val *= (-1);
	if(val > THRESHOLD_MAX)	val = THRESHOLD_MAX;
		
	hkc1xx_touch.threshold_x = val;
    
    local_irq_restore(flags);

    return count;
}

//[*]--------------------------------------------------------------------------------------------------[*]
static 	ssize_t show_threshold_y		(struct device *dev, struct device_attribute *attr, char *buf)
{
	if(hkc1xx_touch.threshold_y > THRESHOLD_MAX)	hkc1xx_touch.threshold_y = THRESHOLD_MAX;
		
	return	sprintf(buf, "%d\n", hkc1xx_touch.threshold_y);
}

//[*]--------------------------------------------------------------------------------------------------[*]
static 	ssize_t set_threshold_y			(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    unsigned long 	flags;
    unsigned int	val;

    if(!(sscanf(buf, "%u\n", &val)))	return	-EINVAL;

    local_irq_save(flags);
    
	if(val < 0)				val *= (-1);
	if(val > THRESHOLD_MAX)	val = THRESHOLD_MAX;
		
	hkc1xx_touch.threshold_y = val;
    
    local_irq_restore(flags);

    return count;
}

//[*]--------------------------------------------------------------------------------------------------[*]

#if defined(CONFIG_TOUCHSCREEN_KETI)
//[*]--------------------------------------------------------------------------------------------------[*]
static 	ssize_t show_threshold_z		(struct device *dev, struct device_attribute *attr, char *buf)
{
	if(hkc1xx_touch.threshold_z > TS_PRESS_MAX)	hkc1xx_touch.threshold_z = TS_PRESS_MAX;
		
	return	sprintf(buf, "%d\n", hkc1xx_touch.threshold_z);
}

//[*]--------------------------------------------------------------------------------------------------[*]
static 	ssize_t set_threshold_z			(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    unsigned long 	flags;
    unsigned int	val;

    if(!(sscanf(buf, "%u\n", &val)))	return	-EINVAL;

    local_irq_save(flags);

	if(val < 0)				val *= (-1);
	if(val > TS_PRESS_MAX)	val = TS_PRESS_MAX;
		
	hkc1xx_touch.threshold_z = val;
    
    local_irq_restore(flags);

    return count;
}

//[*]--------------------------------------------------------------------------------------------------[*]
static 	ssize_t show_debug		(struct device *dev, struct device_attribute *attr, char *buf)
{
	if(hkc1xx_touch.debug > 1)	hkc1xx_touch.debug = 1;
		
	return	sprintf(buf, "%d\n", hkc1xx_touch.debug);
}

//[*]--------------------------------------------------------------------------------------------------[*]
static 	ssize_t set_debug			(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    unsigned long 	flags;
    unsigned int	val;

    if(!(sscanf(buf, "%u\n", &val)))	return	-EINVAL;

    local_irq_save(flags);

	if(val > 0)		val = 1;
	else			val = 0;
		
	hkc1xx_touch.debug = val;
    
    local_irq_restore(flags);

    return count;
}

#endif

#if (defined(CONFIG_TOUCHSCREEN_ODROID_MT_T)||defined(CONFIG_TOUCHSCREEN_X10)||defined(CONFIG_TOUCHSCREEN_ODROID_MT_7)||defined(CONFIG_TOUCHSCREEN_ODROID_MT_7_P))
//[*]--------------------------------------------------------------------------------------------------[*]
//[*]--------------------------------------------------------------------------------------------------[*]
static	ssize_t set_ts_cal		(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	unsigned char 	wdata;
    unsigned long 	flags;
    unsigned int	val;

    if(!(sscanf(buf, "%u\n", &val)))	return	-EINVAL;

    local_irq_save(flags);		local_irq_disable();

	#if defined(CONFIG_TOUCHSCREEN_ODROID_MT_7) || defined(CONFIG_TOUCHSCREEN_ODROID_MT_7_P)
		// touch recalibration
		wdata =0x00;
		hkc1xx_touch_write(TS_RECALIBRATION, &wdata, 0);	// set mode
		
		mdelay(500);
	#else
		// INT_mode : disable interrupt
		wdata = 0x00;
		hkc1xx_touch_write(MODULE_INTMODE, &wdata, 1);
	
		// touch calibration
		wdata =0x03;
		hkc1xx_touch_write(MODULE_CALIBRATION, &wdata, 1);	// set mode
		
		mdelay(500);
	
		// INT_mode : enable interrupt, low-active, periodically
		wdata = 0x09;
		hkc1xx_touch_write(MODULE_INTMODE, &wdata, 1);
	#endif

    local_irq_restore(flags);

    return count;
}

//[*]--------------------------------------------------------------------------------------------------[*]
//[*]--------------------------------------------------------------------------------------------------[*]
#if defined(CONFIG_TOUCHSCREEN_ODROID_MT_7) || defined(CONFIG_TOUCHSCREEN_ODROID_MT_7_P)
static	ssize_t set_ts_sense		(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	unsigned char 	wdata;
	unsigned long 	flags;
	unsigned int	val;

    if(!(sscanf(buf, "%u\n", &val)))	return	-EINVAL;

    local_irq_save(flags);		local_irq_disable();

	// touch sensitivity
	wdata = (unsigned char)(val&0xff);
	hkc1xx_touch_write(TS_SENSITIVITY_CTL, &wdata, 1);
	
    local_irq_restore(flags);

	return count;
}

//[*]--------------------------------------------------------------------------------------------------[*]
//[*]--------------------------------------------------------------------------------------------------[*]
static 	ssize_t show_ts_sense		(struct device *dev, struct device_attribute *attr, char *buf)
{
	unsigned char 	rdata[4];
	unsigned long 	flags;

    local_irq_save(flags);		local_irq_disable();

	hkc1xx_touch_write(TS_SENSITIVITY_CTL, NULL, 0);	// set mode
	hkc1xx_touch_read( rdata, 2);

    local_irq_restore(flags);
    
    return	sprintf(buf, "touch sensitivity : 0x%02x\n", rdata[1]);
}

//[*]--------------------------------------------------------------------------------------------------[*]
//[*]--------------------------------------------------------------------------------------------------[*]
static	ssize_t set_ts_enable		(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	unsigned long 	flags;

    local_irq_save(flags);		local_irq_disable();

	if(!strcmp(buf, "on\n"))		hkc1xx_touch.enable = 1;
	else							hkc1xx_touch.enable = 0;

    local_irq_restore(flags);

	return count;
}

//[*]--------------------------------------------------------------------------------------------------[*]
//[*]--------------------------------------------------------------------------------------------------[*]
static 	ssize_t show_ts_enable		(struct device *dev, struct device_attribute *attr, char *buf)
{
	if(hkc1xx_touch.enable)		return	sprintf(buf, "on\n");
	else						return	sprintf(buf, "off\n");
}

//[*]--------------------------------------------------------------------------------------------------[*]
//[*]--------------------------------------------------------------------------------------------------[*]
static	ssize_t set_ts_key_enable		(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	unsigned long 	flags;

    local_irq_save(flags);		local_irq_disable();

	if(!strcmp(buf, "on\n"))		hkc1xx_touch.key_enable = 1;
	else							hkc1xx_touch.key_enable = 0;

    local_irq_restore(flags);

	return count;
}

//[*]--------------------------------------------------------------------------------------------------[*]
//[*]--------------------------------------------------------------------------------------------------[*]
static 	ssize_t show_ts_key_enable		(struct device *dev, struct device_attribute *attr, char *buf)
{
	if(hkc1xx_touch.key_enable)		return	sprintf(buf, "on\n");
	else							return	sprintf(buf, "off\n");
}

//[*]--------------------------------------------------------------------------------------------------[*]
//[*]--------------------------------------------------------------------------------------------------[*]
void	hkc1xx_touch_fw_upgrade_check	(void)
{
	hkc1xx_touch.fw_size = DEFAULT_FW_SIZE;

	memcpy(hkc1xx_touch.fw, &DEFAULT_FW_CODE[0], hkc1xx_touch.fw_size);
	
	if(GET_JOG_STATUS())	hkc1xx_touch.fw_ver = -1;	// force upgrade

	hkc1xx_touch_fw_upgrade();
}

//[*]--------------------------------------------------------------------------------------------------[*]
static void	upgrade_timer_function(unsigned long arg)
{
	unsigned long	flags;
	
	local_irq_save(flags);	local_irq_disable();

	if(hkc1xx_touch_fw_upgrade() != 0)	upgrade_status = 0;
	else								upgrade_status = 2;
		
	// change normal mode
	hkc1xx_touch_mode_change(TOUCH_MODE_NORMAL);

	local_irq_restore(flags);
}

//[*]--------------------------------------------------------------------------------------------------[*]
static	ssize_t set_ts_upgrade			(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	unsigned long 	flags;
    unsigned int	val;

    local_irq_save(flags);		local_irq_disable();

    if(!(sscanf(buf, "%u\n", &val)))	return	-EINVAL;

	switch(val)	{
		default	:	case	0:
			upgrade_status = 0;		hkc1xx_touch.fw_size = 0;	
			memset(hkc1xx_touch.fw, 0xFF, MAX_FW_SIZE);
			break;
			
		case	1:	case	2:
			if(upgrade_status != 1)	{
				if(val != 1)	{	// default f/w write
					hkc1xx_touch.fw_size = DEFAULT_FW_SIZE;
					memcpy(hkc1xx_touch.fw, &DEFAULT_FW_CODE[0], hkc1xx_touch.fw_size);
				}

				if(hkc1xx_touch.fw_size > MIN_FW_SIZE)	{
					// run_upgrade_timer
					del_timer_sync(&upgrade_timer);		init_timer(&upgrade_timer);
					
					upgrade_timer.data 		= (unsigned long)&upgrade_timer;
					upgrade_timer.function 	= upgrade_timer_function;
					upgrade_timer.expires 	= jiffies + HZ/10;
					
					add_timer(&upgrade_timer);
		
					upgrade_status = 1;	// start upgrading
				}
				else	{
					printk("\nNo Firmware Data (fw_size = 0)\n");
				}
			}
			break;
	}

    local_irq_restore(flags);

	return count;
}

//[*]--------------------------------------------------------------------------------------------------[*]
//[*]--------------------------------------------------------------------------------------------------[*]
static 	ssize_t show_ts_upgrade			(struct device *dev, struct device_attribute *attr, char *buf)
{
	switch(upgrade_status)	{
		default	:		upgrade_status = 0;
		case	0:		return	sprintf(buf, "No Firmware Upgrade.\n");
		case	1:		return	sprintf(buf, "Firmware Upgrading...\n");
		case	2:		return	sprintf(buf, "Firmware Upgrade Complete.\n");
	}
}

//[*]--------------------------------------------------------------------------------------------------[*]
//[*]--------------------------------------------------------------------------------------------------[*]
static	ssize_t set_ts_upgrade_data		(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	unsigned long 	flags;

    local_irq_save(flags);		local_irq_disable();

	if(hkc1xx_touch.fw != NULL)	{
		memcpy(&hkc1xx_touch.fw[hkc1xx_touch.fw_size], buf, count);
		hkc1xx_touch.fw_size += count;
	}
	
    local_irq_restore(flags);

	return count;
}

//[*]--------------------------------------------------------------------------------------------------[*]
//[*]--------------------------------------------------------------------------------------------------[*]
static 	ssize_t show_ts_upgrade_data	(struct device *dev, struct device_attribute *attr, char *buf)
{
	return	sprintf(buf, "Download Data Size = 0x%04X\n", hkc1xx_touch.fw_size);
}

//[*]--------------------------------------------------------------------------------------------------[*]
//[*]--------------------------------------------------------------------------------------------------[*]
static	ssize_t set_ts_firmware_info	(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	printk("Do not support this function\n");
	
	return count;
}

//[*]--------------------------------------------------------------------------------------------------[*]
//[*]--------------------------------------------------------------------------------------------------[*]
static 	ssize_t show_ts_firmware_info	(struct device *dev, struct device_attribute *attr, char *buf)
{
	return	sprintf(buf, "Firmware Info [ Ver : 0x%02X, Rev : 0x%02X ]\n", hkc1xx_touch.fw_ver, hkc1xx_touch.fw_rev);
}

//[*]--------------------------------------------------------------------------------------------------[*]
//[*]--------------------------------------------------------------------------------------------------[*]
#endif	//#if defined(CONFIG_TOUCHSCREEN_ODROID_MT_7) || defined(CONFIG_TOUCHSCREEN_ODROID_MT_7_P)
#endif	//#if (defined(CONFIG_TOUCHSCREEN_ODROID_MT_T)||defined(CONFIG_TOUCHSCREEN_X10)||defined(CONFIG_TOUCHSCREEN_ODROID_MT_7)||defined(CONFIG_TOUCHSCREEN_ODROID_MT_7_P))

//[*]--------------------------------------------------------------------------------------------------[*]
int		hkc1xx_touch_sysfs_create		(struct platform_device *pdev)	
{
	// variable init
	hkc1xx_touch.hold_status	= 0;

#if defined(CONFIG_TOUCHSCREEN_ODROID_MT_7) || defined(CONFIG_TOUCHSCREEN_ODROID_MT_7_P)
	hkc1xx_touch.sampling_rate	= 1;	// 20 msec sampling
	hkc1xx_touch.enable			= 0;	// Default disable
	hkc1xx_touch.key_enable		= 0;	// Default disable

	hkc1xx_touch.fw 			= NULL;;
	hkc1xx_touch.fw_size 		= 0;
#else
	#if defined(CONFIG_TOUCHSCREEN_ODROID_MT_SMD)
		hkc1xx_touch.sampling_rate	= 1;	// 20 msec sampling
	#else
		hkc1xx_touch.sampling_rate	= 0;	// 10 msec sampling
	#endif
#endif
	hkc1xx_touch.threshold_x	= TS_X_THRESHOLD;	// x data threshold (0~10)
	hkc1xx_touch.threshold_y	= TS_Y_THRESHOLD;	// y data threshold (0~10)

#if defined(CONFIG_TOUCHSCREEN_KETI)
	hkc1xx_touch.threshold_z	= TS_Z_THRESHOLD;	// z data threshold (0~2000)
//	hkc1xx_touch.debug			= 1;
	hkc1xx_touch.debug			= 0;
#endif	

	return	sysfs_create_group(&pdev->dev.kobj, &hkc1xx_touch_attr_group);
}

//[*]--------------------------------------------------------------------------------------------------[*]
void	hkc1xx_touch_sysfs_remove		(struct platform_device *pdev)	
{
    sysfs_remove_group(&pdev->dev.kobj, &hkc1xx_touch_attr_group);
}
//[*]--------------------------------------------------------------------------------------------------[*]
//[*]--------------------------------------------------------------------------------------------------[*]

