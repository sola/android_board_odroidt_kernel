/* linux/drivers/video/samsung/s3cfb_smd1024x600.c
 * 7.0" WSVGA Landscape LCD module driver for the HKDKC110
*/

#include "s3cfb.h"

static struct s3cfb_lcd smd1024x600 = {
	.width	= 1024,
	.height	= 600,
	.bpp	= 32,
	.freq	= 60,

	.timing = {
		.h_bp	= 144,
		.h_fp	= 48,
		.h_sw	= 30,
		.v_fp	= 8,
		.v_bp	= 8,
		.v_fpe	= 1,
		.v_bpe	= 1,
		.v_sw	= 3,
	},

	.polarity = {
		.rise_vclk	= 0,
		.inv_hsync	= 1,
		.inv_vsync	= 1,
		.inv_vden	= 0,
	},
};

/* name should be fixed as 's3cfb_set_lcd_info' */
void s3cfb_set_lcd_info(struct s3cfb_global *ctrl)
{
	smd1024x600.init_ldi = NULL;
	ctrl->lcd = &smd1024x600;
}
